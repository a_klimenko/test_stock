<?php

require_once 'Stock.php';

$stock = new Stock();

$stock->readData();

$stock->resolve();

$stock->saveResult();

$outArray = $stock->getOutput();

print_r($outArray);
