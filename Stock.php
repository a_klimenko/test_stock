<?php
/**
 * Class to resolve stock task
 *
 * @author Alexey
 */
class Stock {

    private $columns;
    private $orderBy;
    private $input;
    private $output;
    
    
    /**
     * Constructor of class
     */
    function __construct($p_columns = ['provider', 'stock', 'terminal', 'sort', 'countBoxes', 'countItems'],
            $p_orderBy = ['provider' => 'asc', 'stock' => 'asc', 'sort' => 'asc',]) 
    {
        $this->columns = $p_columns;
        $this->orderBy = $p_orderBy;
        $this->input = array();
        $this->output = array();
    }
    
    /**
     * Read data from file
     * 
     * @param string $filename
     */
    public function readData($filename = 'data/2018-12-01.txt') {
        $lines = file($filename);
        foreach ($lines as $line_num => $line) {
            $array = explode(',', $line);
            foreach ($this->columns as $key => $value) {
                $this->input[$line_num][$value] = $array[$key];
            }
        }
    }
    
    /**
     * Main function. Sort income data and form output array
     * 
     */
    public function resolve() {
        
        //Sort the array received from the file
        uasort($this->input, function ($a, $b) {
            $result = 0;

            foreach ($this->orderBy as $key => $value) {

                if ($a[$key] == $b[$key]) {
                    continue;
                }

                $result = ($a[$key] < $b[$key]) ? -1 : 1;

                if ($value == 'desc') {
                    $result = -$result;
                }

                break;
            }

            return $result;
        });
        
        //Reindex array
        $this->input = array_values($this->input);

        foreach ($this->input as $item) {
            $lastKey = count($this->output) - 1;
            $condition = $item['provider'] == $this->output[$lastKey]['provider'] && $item['stock'] == $this->output[$lastKey]['stock'] && $item['sort'] == $this->output[$lastKey]['sort'];

            if ($condition) {
                $this->output[$lastKey]['countBoxes'] = $this->output[$lastKey]['countBoxes'] + $item['countBoxes'];
                $this->output[$lastKey]['countItems'] = $this->output[$lastKey]['countItems'] + $item['countItems'];
            } else {
                $this->output[] = $item;
            }
        }
    }
    
    /**
     * Save result in file
     * 
     * @param string $filename
     */
    public function saveResult($filename = 'data/2018-12-01.result.txt') {
        foreach ($this->output as $key => $item) {
            $content = $content . "\r" . $item['provider'] . ',' . $item['stock'] . ','
                    . $item['sort'] . ',' . $item['countBoxes'] . ',' . $item['countItems'];
            if ($key == 0)
                $content = $content . "\r\n";
        }
        file_put_contents($filename, $content);
    }
    
    /**
     * Get output array
     * 
     * @return array
     */
    public function getOutput() {
        return $this->output;
    }
}
